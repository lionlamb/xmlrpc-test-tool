# xmlrpc-test-tool #
web-based tool, which provides a frontend for testing a xmlrpc-interface

[Online-Demo](http://www.tomhost.de/dev/tools/xmlrpc-tt/)

[Download ZIP (v0.5.2)](https://bitbucket.org/srccode/xmlrpc-test-tool/downloads/xmlrpc_test.0-5-2.zip)

### Features ###
* simple, fast and intuitive html/ajax-interface
* supports an unlimited number of parameters
* supports all xmlrpc-compatible interfaces
* can receive all list with all xmlrpc-methods provied by server (if supported)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Requirements ###

* PHP-Interpreter (>5.2)
* up-to-date browser with activated javascript

### Librarys and plugins which are included/used ###

* [XML-RPC for PHP](http://phpxmlrpc.sourceforge.net/) (v3.0.0beta)
* [JQuery](http://jquery.com/) (v1.4)
* [JQuery UI](http://jqueryui.com/) (v1.8.2)

### Screenshots ###
![screenshot_051.png](https://bitbucket.org/repo/jy9MbB/images/3692548021-screenshot_051.png)
![screenshot_051_2.png](https://bitbucket.org/repo/jy9MbB/images/87450843-screenshot_051_2.png)